const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const PORT = 3000;
const HOST = '0.0.0.0';

const app = express();

const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const pagina = require('./routes/pagina');

app.use(bodyParser.urlencoded({extended: false}));

app.use('/admin',adminRoutes);
app.use(shopRoutes);
app.use(pagina);


app.use((req, res, next) =>{
  res.status(404).sendFile(path.join(__dirname, 'views','404.html'));
});

app.listen(PORT, HOST);