const path = require('path');
const express = require('express');
const router = express.Router();

router.get('/add-product', (req, res, next) => {
    res.sendFile(path.join(__dirname,'../','views', 'add-product.html'));
  });
  
router.post('/admin/add-product' , (req, res, next) => {
    console.log(req.body);
    res.redirect('/');
});

router.get('/pagina', (req, res, next) => {
  res.sendFile(path.join(__dirname,'../','views', 'pagina.html'));
});

router.post('/admin/pagina' , (req, res, next) => {
  console.log(req.body);
  res.redirect('/');
});

module.exports = router;